{*
 +--------------------------------------------------------------------+
 | CiviCRM version 3.4                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2011                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*}
<div class="crm-form-block crm-block crm-contact-task-createsmartgroup-form-block">
<h3>{ts}Smart Group{/ts}</h3>
    {if $qill[0]}
        <div id="search-status">
            <ul>
                {foreach from=$qill[0] item=criteria}
                    <li>{$criteria}</li>
                {/foreach}
            </ul>
            <br />
        </div>
    {/if}
  <table class="form-layout-compressed">
        <tr class="crm-contact-task-createsmartgroup-form-block-title">
            <td class="label">{$form.title.label}</td>
            <td>{$form.title.html}</td>
        </tr>
	<tr class="crm-contact-task-createsmartgroup-form-block-description">
            <td class="label">{$form.description.label}</td>
            <td>{$form.description.html}</td>
        </tr>
          {if $form.group_type}
        <tr class="crm-contact-task-createsmartgroup-form-block-group_type"> 
            <td class="label">{$form.group_type.label}</td>
            <td>{$form.group_type.html}</td>
        </tr>
          {/if}
          {if $form.cpg_lifespan }
            <tr class="crm-group-form-block-cpg-lifespan">
              <td class="label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$form.cpg_lifespan.label}</td>
              <td>{$form.cpg_lifespan.html}</td>
            </tr>
          {/if}
          {if $form.cpg_sharing || $form.cpg_field }
            <tr><td><h3>{ts}Sharing Settings{/ts}</h3></td></tr>
            {if $form.cpg_sharing }
              <tr class="crm-group-form-block-sharing">
                <td class="label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$form.cpg_sharing.label}</td>
                <td>{$form.cpg_sharing.html}</td>
              </tr>
            {/if}
            {if $form.cpg_field }
              <tr class="crm-group-form-block-cpg-field">
                <td class="label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$form.cpg_field.label}</td>
                <td>{$form.cpg_field.html}</td>
              </tr>
            {/if}
            {if $form.cpg_take_owner}
              <tr class="crm-group-form-block-cpg-owner">
                <td class="label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$form.cpg_take_owner.label}</td>
                <td>{$form.cpg_take_owner.html}</td>
              </tr>
            {/if}
          {/if}
  </table>
 <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="bottom"}</div>
</div>
{if $form.cpg_sharing}{literal}
<script type="text/javascript">
cj(document).ready( function ($) {
  $("input[name='cpg_sharing']").bind('change.cpg', function () {
    if($("input[name='cpg_sharing']:checked").val() == 'shared') {
      $("tr.crm-group-form-block-cpg-field").stop().show();
    } else {
      $("tr.crm-group-form-block-cpg-field").stop().hide();
    }
  }).change();
});
</script>
{/literal}{/if}
